# -*- coding: utf-8 -*-

class TaskState(object):
    CREATED = 0
    WORKING = 1
    COMPLETED = 2
