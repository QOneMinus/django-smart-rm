# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.db import models
from .task_states import TaskState

from srm.file_operations import get_full_path


class WasteBasket(models.Model):
    name = models.CharField(max_length=50, default="WasteBasket")

    rmdir    = models.BooleanField()
    force    = models.BooleanField()
    dry_run  = models.BooleanField()

    max_size            = models.IntegerField(default=32)
    storage_time        = models.DurationField(default=datetime.timedelta(days=30))
    #wastebasket_path    = models.FilePathField(path=get_full_path('~/'), allow_files=False, allow_folders=True)
    wastebasket_path    = models.TextField(default='~/Trash')

    def __unicode__(self):
        return self.name


class Task(models.Model):
    name = models.CharField(max_length=50, default="Task")
    state = models.IntegerField(default=TaskState.CREATED)

    wastebasket = models.ForeignKey(WasteBasket)

    rmdir    = models.NullBooleanField()
    force    = models.NullBooleanField()
    dry_run  = models.NullBooleanField()

    restore = models.BooleanField()
    regex   = models.BooleanField()

    paths = models.TextField()

    def __unicode__(self):
        return self.name
